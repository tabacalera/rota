import { supabase } from '../initSupabase';
import { ParkingReservation } from '../types/parking';

export const updateReservation = async (
    id: any,
    changes: Partial<ParkingReservation>
): Promise<{
    success: boolean;
    message: string;
}> => {
    const resp: {
        success: boolean;
        message: string;
        data: any[];
    } = {
        success: true,
        message: '',
        data: [],
    };
    const { data, error } = await supabase
        .from('reservation')
        .update({
            ...changes,
        })
        .eq('id', id)
        .select();

    if (error) {
        resp.success = false;
        resp.message = error.message;
        return resp;
    }

    resp.data = data;
    return resp;
};
