import { supabase } from '../initSupabase';
import { Parking } from '../types/parking';

export const getParkings = async (): Promise<Parking[]> => {
    const { data, error } = await supabase.from('parking').select();
    if (error) {
        return [] as Parking[];
    }
    return data as Parking[];
};

export const getParking = async (id: string): Promise<Parking | undefined> => {
    const { data, error } = await supabase.from('parking').select().eq('id', id);
    if (error || !data[0]) {
        return undefined;
    }
    return data[0] as Parking;
};
