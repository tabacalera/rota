import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import SecondScreen from '../screens/SecondScreen';
import MainTabs from './MainTabs';
import { Layout, Text, themeColor, TopNav, useTheme } from 'react-native-rapi-ui';
import { View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AddParkingSpace from '../screens/ParkingSpaces/AddParkingSpace';
import AddParkingReservationScreen from '../screens/Reservation/AddParkingReservationScreen';
import ParkingPage from '../screens/ParkingSpaces/ParkingPage';
import { BookingList } from '../screens/Booking/BookingList';
import ScanReservationScreen from '../screens/Reservation/ScanReservationScreen';
import UserHistoryScreen from '../screens/Reservation/UserHistoryScreen';

const MainStack = createNativeStackNavigator();
const Main = () => {
    const { isDarkmode, setTheme } = useTheme();
    return (
        <MainStack.Navigator
            screenOptions={{
                headerShown: false,
            }}
        >
            <MainStack.Screen name="Home" component={MainTabs} />
            {/* @ts-ignore */}
            <MainStack.Screen name="SecondScreen" component={SecondScreen} />
            {/* @ts-ignore */}
            <MainStack.Screen name="AddParkingLotScreen" component={AddParkingSpace} />
            {/* @ts-ignore */}
            <MainStack.Screen name="ParkingPageScreen" component={ParkingPage} />
            {/* @ts-ignore */}
            <MainStack.Screen name="AddReservationScreen" component={AddParkingReservationScreen} />
            {/* @ts-ignore */}
            <MainStack.Screen name="ScanReservationScreen" component={ScanReservationScreen} />
            {/* @ts-ignore */}
            <MainStack.Screen name="UserHistoryScreen" component={UserHistoryScreen} />
            {/* <MainStack.Screen name="BookingListScreen" component={BookingList} /> */}
        </MainStack.Navigator>
    );
};

export default Main;
