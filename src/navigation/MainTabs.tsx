import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { themeColor, useTheme } from 'react-native-rapi-ui';
import TabBarIcon from '../components/utils/TabBarIcon';
import TabBarText from '../components/utils/TabBarText';

import Home from '../screens/Home';
import About from '../screens/About';
import Profile from '../screens/Profile';
import Reservation from '../screens/Reservation/Reservation';
import { Ionicons } from '@expo/vector-icons';
import { useAuth } from '../provider/AuthProvider';

const Tabs = createBottomTabNavigator();
const MainTabs = () => {
    const { isDarkmode } = useTheme();
    const { userData } = useAuth();
    return (
        <Tabs.Navigator
            screenOptions={{
                headerShown: false,
                tabBarStyle: {
                    borderTopColor: isDarkmode ? themeColor.dark100 : '#c0c0c0',
                    backgroundColor: isDarkmode ? themeColor.dark200 : '#ffffff',
                },
            }}
        >
            {/* these icons using Ionicons */}
            <Tabs.Screen
                name="ParkingLotsScreen"
                // @ts-ignore
                component={Home}
                options={{
                    tabBarLabel: ({ focused }: any) => (
                        <TabBarText focused={focused} title="Parking Spaces" />
                    ),
                    tabBarIcon: ({ focused }: any) => (
                        <TabBarIcon focused={focused} icon={'md-home'} />
                    ),
                }}
            />

            <Tabs.Screen
                name="Reservation"
                // @ts-ignore
                component={Reservation}
                options={{
                    tabBarLabel: ({ focused }: any) => (
                        <TabBarText
                            focused={focused}
                            title={
                                userData && userData.role === 'driver'
                                    ? 'My Reservations'
                                    : 'Reservations'
                            }
                        />
                    ),
                    tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} icon="list" />,
                }}
            />

            {/* <Tabs.Screen
        name="About"
        component={About}
        options={{
          tabBarLabel: ({ focused }) => (
            <TabBarText focused={focused} title="About" />
          ),
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} icon={"ios-information-circle"} />
          ),
        }}
      /> */}
        </Tabs.Navigator>
    );
};

export default MainTabs;
