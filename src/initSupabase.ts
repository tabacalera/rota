import AsyncStorage from '@react-native-async-storage/async-storage';
import { createClient } from '@supabase/supabase-js';

// Better put your these secret keys in .env file
export const supabase = createClient(
    'https://xjqnxxivjjjmbblaczlg.supabase.co',
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InhqcW54eGl2ampqbWJibGFjemxnIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzQ0NzY1MzgsImV4cCI6MTk5MDA1MjUzOH0.eavNTSOwkCrv5QtuixwxEEcO42Zm5j1nbeGUzD3d8Ew',
    {
        localStorage: AsyncStorage as any,
        detectSessionInUrl: false, // Prevents Supabase from evaluating window.location.href, breaking mobile
    }
);
