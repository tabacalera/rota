import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { Layout, TopNav, themeColor, useTheme } from 'react-native-rapi-ui';
import { Ionicons } from '@expo/vector-icons';
import { MainStackParamList } from '../../types/navigation';
import { Modal, Form, FormItem, Label } from 'react-native-form-component';
import { supabase } from '../../initSupabase';
import * as DocumentPicker from 'expo-document-picker';
import moment from 'moment';
import { Parking } from '../../types/parking';
import { getParking } from '../../data/parking.actions';
import { Card, Image, Button } from 'react-native-elements';
import { useAuth, UserData } from '../../provider/AuthProvider';
const nameValue: {
    [key: string]: any;
} = {
    name: 'Name',
    address: 'Address',
    numberOfSlots: 'Parking slots',
    pricePerHour: 'Price',
};
const AddParkingSpaceScreen = function ({
    navigation,
    route,
}: NativeStackScreenProps<MainStackParamList, 'AddParkingLotScreen'>) {
    const { isDarkmode } = useTheme();
    const [showImage, setShowImage] = React.useState(false);
    const [currentImage, setCurrentImage] = React.useState({
        uri: '',
        name: '',
    });

    const { userData }: any = useAuth();
    const { role } = userData;
    const userRole: UserData['role'] = role || 'driver';
    const [error, setError] = React.useState<{
        [key: string]: any;
    }>({
        show: true,
        message: '',
    });
    const [values, setValues] = React.useState<{
        [key: string]: any;
    }>({
        name: '',
        numberOfSlots: '',
        pricePerHour: '',
        address: '',
    });
    const [isEdit, setIsEdit] = React.useState(false);
    const [fileResponse, setFileResponse] = React.useState<any>([]);
    const [parkingId, setParkingId] = React.useState('');
    const onChange = (name: string, value: any) => {
        setValues({
            ...values,
            [name]: value,
        });
    };

    const onSubmit = async () => {
        const status = validate();
        if (status) return;
        const payload: Parking = {
            pricePerHour: parseInt(values.pricePerHour),
            numberOfSlots: parseInt(values.numberOfSlots),
            creation: moment().unix(),
            availableSlots: parseInt(values.numberOfSlots),
            name: values.name,
            address: values.address,
        };
        const { data, error } = await supabase.from('parking').insert(payload);
        if (error) {
            setError({
                show: true,
                message: error.message,
            });
            return;
        }
        navigation.navigate('ParkingLotsScreen');
    };

    const onUpdate = async () => {
        const status = validate();
        if (status) return;
        const payload: Parking = {
            pricePerHour: parseInt(values.pricePerHour),
            numberOfSlots: parseInt(values.numberOfSlots),
            creation: moment().unix(),
            availableSlots: parseInt(values.numberOfSlots),
            name: values.name,
            address: values.address,
        };
        await supabase.from('parking').update(payload).eq('id', parkingId);
        navigation.navigate('ParkingLotsScreen');
    };

    const validate = () => {
        const err = {
            show: false,
            message: '',
        };
        const keys = Object.keys(values);
        keys.some((key: string) => {
            const value = values[key];
            if (!value) {
                err.show = true;
                err.message = `${nameValue[key]} is required.`;
                return true;
            }
        });
        setError(err);
        return err.show ? err.message : false;
    };

    const handleDeleteImage = async (img: any) => {
        const { error } = await supabase.from('files').delete().eq('id', img.id);
        if (!error) {
            setCurrentImage({
                uri: '',
                name: '',
            });
            setShowImage(false);
            const newFiles = [...fileResponse];
            newFiles.splice(img.fileIdx, 1);
            setFileResponse(newFiles);
        }
    };
    const handleDocumentSelection = async () => {
        try {
            const response = await DocumentPicker.getDocumentAsync({
                multiple: true,
                type: ['image/*'],
            });
            if (response.type === 'success') {
                const newName = `public/${moment().format('MM-DD-YYYY-hh-mm-ss-a')}-${
                    response.name
                }`;
                const { data, error } = await supabase.storage
                    .from('payments')
                    .upload(newName, response as any, {
                        contentType: response.mimeType,
                    });
                if (error) {
                    setError({
                        show: true,
                        message: error.message,
                    });
                    return;
                }
                if (!data) return;
                const url = supabase.storage.from('payments').getPublicUrl(newName);
                const dataFile = {
                    uri: url.data?.publicURL,
                    name: newName,
                    path: data.Key,
                    creation: moment().unix(),
                    uploadedBy: supabase.auth.user()?.id,
                    parkingId: values.parkingId,
                };
                console.log(dataFile);
                const resp = await supabase.from('parkingFiles').insert(dataFile);
                console.log(resp);
                if (resp.error) return;
                const newFiles = [...fileResponse, resp.data[0]];
                setFileResponse(newFiles);
            }
        } catch (err) {
            console.warn(err);
        }
    };
    const getParkingData = async (id: string) => {
        const parking = await getParking(id);
        if (parking) {
            setValues({
                // @ts-ignore
                ...route.params,
                ...parking,
            });
            setIsEdit(true);
            const respFiles = await supabase.from('parkingFiles').select().eq('parkingId', id);
            console.log(respFiles);
            setFileResponse(respFiles.data);
        }
    };

    React.useEffect(() => {
        console.log(route.params);
        // @ts-ignore
        if (route && route.params && route.params.parkingId) {
            // @ts-ignore
            getParkingData(route.params.parkingId);
            // @ts-ignore
            setParkingId(route.params.parkingId);
        } else {
            setIsEdit(false);
        }
    }, [route.params]);
    return (
        <Layout>
            <TopNav
                middleContent={`${isEdit ? `Update Parking Space` : 'Add Parking Space'} `}
                leftContent={
                    <Ionicons
                        name="chevron-back"
                        size={20}
                        color={isDarkmode ? themeColor.white100 : themeColor.dark}
                    />
                }
                leftAction={() => navigation.goBack()}
            />

            <ScrollView>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#fff',
                    }}
                >
                    <Modal
                        show={showImage}
                        visible={showImage}
                        onRequestClose={() => {
                            setCurrentImage({
                                uri: '',
                                name: '',
                            });
                            setShowImage(false);
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                                height: '100%',
                                width: '100%',
                                backgroundColor: 'rgba(0,0,0,0.3)',
                            }}
                        >
                            <Card
                                wrapperStyle={{
                                    height: '80%',
                                    width: '90%',
                                }}
                                containerStyle={{
                                    height: '80%',
                                    width: '90%',
                                    alignItems: 'center',
                                }}
                            >
                                <Card.Title>{currentImage.name}</Card.Title>
                                <Card.Divider />

                                {values.status === 'approved' && userRole === 'driver' ? null : (
                                    <Button
                                        icon={
                                            <Ionicons
                                                name="trash-bin"
                                                color="#fff"
                                                size={18}
                                                style={{ marginRight: 10 }}
                                            />
                                        }
                                        title="Remove Image"
                                        buttonStyle={{
                                            backgroundColor: 'red',
                                            marginBottom: 10,
                                        }}
                                        onPress={() => handleDeleteImage(currentImage)}
                                    />
                                )}
                                <View>
                                    <Card.Image
                                        source={{
                                            uri: currentImage.uri,
                                        }}
                                        style={{
                                            width: '100%',
                                            height: '100%',
                                        }}
                                        resizeMethod="auto"
                                        resizeMode="center"
                                    />
                                </View>
                            </Card>
                        </View>
                    </Modal>
                    <Form
                        buttonText={isEdit ? 'Update' : 'Submit'}
                        buttonStyle={{
                            backgroundColor: '#007bff',
                        }}
                        style={{ marginTop: 24, width: 350, maxWidth: 320 }}
                        onButtonPress={isEdit ? onUpdate : onSubmit}
                    >
                        <FormItem
                            isRequired
                            style={styles.input}
                            onChangeText={(txt: any) => onChange('name', txt)}
                            label="Name"
                            value={values.name}
                            showErrorIcon={false}
                            placeholder="Parking space name"
                        />
                        <FormItem
                            isRequired
                            style={styles.input}
                            onChangeText={(txt: any) => onChange('address', txt)}
                            label="Address"
                            value={values.address}
                            showErrorIcon={false}
                            placeholder="#2050, JP Morgan Street, California"
                        />
                        <FormItem
                            isRequired
                            style={styles.input}
                            onChangeText={(txt: any) => {
                                const val = txt.replace(/[^0-9]/g, '');
                                onChange('numberOfSlots', val);
                            }}
                            value={`${values.numberOfSlots}`}
                            label="Slots"
                            showErrorIcon={false}
                            keyboardType="phone-pad"
                            placeholder="1,100"
                        />
                        <FormItem
                            isRequired
                            style={styles.input}
                            onChangeText={(txt: any) => {
                                const val = txt.replace(/[^0-9]/g, '');
                                onChange('pricePerHour', val);
                            }}
                            value={`${values.pricePerHour}`}
                            label="Price"
                            showErrorIcon={false}
                            placeholder="$250.00"
                            keyboardType="phone-pad"
                        />
                        <View style={{ flexDirection: 'row' }}>
                            <Button
                                type="clear"
                                title="Attach"
                                icon={
                                    <Ionicons
                                        name={'attach'}
                                        size={24}
                                        // color={isDarkmode ? themeColor.white100 : themeColor.dark}
                                    />
                                }
                                onPress={handleDocumentSelection}
                            />
                        </View>
                        <View
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                                flexDirection: 'row',
                            }}
                        >
                            {fileResponse.map((file: any, idx: number) => (
                                <Image
                                    key={`${file.name}-${file.uri}`}
                                    source={{
                                        uri: file.uri,
                                    }}
                                    style={{
                                        width: 100,
                                        height: 100,
                                    }}
                                    resizeMode="contain"
                                    onPress={() => {
                                        setShowImage(true);
                                        file.fileIdx = idx;
                                        setCurrentImage(file);
                                    }}
                                />
                            ))}
                        </View>
                        <Label text={error.message} textStyle={{ color: 'red' }} />
                    </Form>
                </View>
            </ScrollView>
        </Layout>
    );
};

export default AddParkingSpaceScreen;
const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    },
    container: {
        flex: 1,
    },
    inner: {
        padding: 24,
        flex: 1,
        justifyContent: 'space-around',
    },
    header: {
        fontSize: 36,
        marginBottom: 48,
    },
    textInput: {
        height: 40,
        borderColor: '#000000',
        borderBottomWidth: 1,
        marginBottom: 36,
    },
    btnContainer: {
        backgroundColor: 'white',
        marginTop: 12,
    },
});
