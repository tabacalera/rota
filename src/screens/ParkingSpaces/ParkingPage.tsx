import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React from 'react';
import { Layout, TopNav } from 'react-native-rapi-ui';
import TabBarIcon from '../../components/utils/TabBarIcon';
import TabBarText from '../../components/utils/TabBarText';
import { MainStackParamList } from '../../types/navigation';
import { BookingList } from '../Booking/BookingList';
import Reservation from '../Reservation/Reservation';
const Tabs = createBottomTabNavigator();

const ParkingPage = ({
  navigation,
  route,
}: NativeStackScreenProps<MainStackParamList, 'ParkingPageScreen'>) => {
  const { parkingId, name }: any = route.params;

  React.useEffect(() => {}, [parkingId]);
  return (
    // <Layout>
    <Tabs.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          borderTopColor: '#c0c0c0',
          backgroundColor: '#ffffff',
        },
      }}
    >
      {/* these icons using Ionicons */}
      <Tabs.Screen
        name="BookingListScreen"
        // @ts-ignore
        component={BookingList}
        options={{
          tabBarLabel: ({ focused }) => <TabBarText focused={focused} title="Bookings" />,
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} icon={'md-book'} />,
        }}
        initialParams={route.params}
      />

      <Tabs.Screen
        name="Reservation"
        // @ts-ignore
        component={Reservation}
        options={{
          tabBarLabel: ({ focused }) => (
            <TabBarText focused={focused} title="Parking Reservations" />
          ),
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} icon={'bookmark'} />,
        }}
        initialParams={{
          parkingData: route.params,
        }}
      />

      {/* <Tabs.Screen
          name="About"
          component={About}
          options={{
            tabBarLabel: ({ focused }) => (
              <TabBarText focused={focused} title="About" />
            ),
            tabBarIcon: ({ focused }) => (
              <TabBarIcon focused={focused} icon={"ios-information-circle"} />
            ),
          }}
        /> */}
    </Tabs.Navigator>
    // </Layout>
  );
};

export default ParkingPage;
