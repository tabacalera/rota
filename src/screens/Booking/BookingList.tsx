import { Ionicons } from '@expo/vector-icons';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import moment, { invalid, isMoment } from 'moment';
import { useAuth } from '../../provider/AuthProvider';
import React from 'react';
import { GestureResponderEvent, ScrollView, View } from 'react-native';
import DateTimePicker, { AndroidEvent } from '@react-native-community/datetimepicker';
import { Badge, BottomSheet, Button, Icon, Input, ListItem, Text } from 'react-native-elements';
import { Layout, TextInput, TopNav } from 'react-native-rapi-ui';
import { supabase } from '../../initSupabase';
import { MainStackParamList } from '../../types/navigation';
import MonthPicker from '../../components/MonthPicker';
import DateFilter from '../../components/DateFilter';
import { Label } from 'react-native-form-component';

export const BookingList = ({
    navigation,
    route,
}: NativeStackScreenProps<MainStackParamList, 'BookingListScreen'>) => {
    const { parkingId, name }: any = route.params;
    const ref = React.useRef();
    const [bookings, setBookings] = React.useState<any[]>([]);
    const [bookingSearch, setBookingSearch] = React.useState<any[]>([]);
    const [search, setSearch] = React.useState<string>('');
    const [filter, setFilter] = React.useState<boolean>(false);
    const [month, setMonth] = React.useState<string | undefined>(undefined);
    const [year, setYear] = React.useState<string | undefined>(undefined);
    const [date, setDate] = React.useState<string | undefined>(undefined);
    const [showFilter, setShowFilter] = React.useState(false);
    const [showMonthPicker, setShowMonthPicker] = React.useState(false);
    const [showYearPicker, setShowYearPicker] = React.useState(false);
    const { userData }: any = useAuth();
    const handlePress = (booking: any) => {
        navigation.navigate('AddReservationScreen', { reservation: booking.reservation } as any);
    };

    const handlePressUser = (e: GestureResponderEvent, user: any) => {
        e.preventDefault();
        // @ts-ignore
        navigation.navigate('UserHistoryScreen', {
            user,
        });
    };
    const updateSearch = (search: string) => {
        setSearch(search);
    };
    const handleMonthChange = (date: string) => {
        setShowMonthPicker(false);
    };
    const getBookingDataByUser = async () => {
        const { data, error }: any = await supabase
            .from('booking')
            .select()

            .eq('parkingId', parkingId)
            .match({
                bookedBy: userData.id,
            }).select(`
              *,
              parking: parkingId(*),
              reservation: reservationId(*),
              user: bookedBy(*)
            `);
        if (error) {
            console.log(error);
            return;
        }
        setBookings(data);
    };

    const getBookingData = async () => {
        const { data, error }: any = await supabase
            .from('booking')
            .select()

            .eq('parkingId', parkingId).select(`
              *,
              parking: parkingId(*),
              reservation: reservationId(*),
              user: bookedBy(*)
            `);
        if (error) {
            console.log(error);
            return;
        }
        setBookings(data);
    };
    const onYearPress = (date: string) => {
        setYear(date);
        setShowYearPicker(false);
    };
    const onMonthPress = (date: string) => {
        setMonth(date);
        setShowMonthPicker(false);
    };
    const onDatePress = (date: string) => {
        setDate(date);
    };

    const handleCancelFilter = () => {
        setShowFilter(false);
    };

    const clearMonth = () => {
        setMonth(undefined);
    };
    const clearYear = () => {
        setYear(undefined);
    };

    const clearDate = () => {
        setDate(undefined);
    };

    const onApply = async () => {
        console.log(year, month, date, year && month === undefined && date);
        if (!year) return true;
        if (year && month === undefined && date) return true;
        let creation = moment();
        if (year) {
            creation = moment(`${year}`).startOf('month');
        }

        if (month !== undefined && !date) {
            creation.set('month', parseInt(month));
        }

        if (month !== undefined && date) {
            creation.set('month', parseInt(month)).set('date', parseInt(date));
        }
        // if (year && month && date) {
        //     creation = moment().set('month', parseInt(month)).set('D', parseInt(date));
        // }
        // setShowFilter(false);
        const from = creation.unix();
        const to = moment().unix();
        console.log(
            from,
            to,
            moment.unix(from).format('MM/DD/YYYY'),
            moment.unix(to).format('MM/DD/YYYY')
        );
        const { data, error } = await supabase
            .from('booking')
            .select(
                `
              *,
              parking: parkingId(*),
              reservation: reservationId(*),
              user: bookedBy(*)
            `
            )
            .eq('parkingId', parkingId)
            .lt('creation', to)
            .gte('creation', from);
        console.log(data, error);
        if (data) {
            setBookingSearch(data);
        }
        setShowFilter(false);
        setFilter(true);
        return false;
        // return false;
    };

    React.useEffect(() => {
        if (userData.role === 'driver') {
            getBookingDataByUser();
            return;
        }
        getBookingData();
    }, [parkingId, userData.id, userData.role]);

    React.useEffect(() => {
        setBookingSearch(bookings);
    }, [bookings]);
    const [bookingSummary, setBookingSummary] = React.useState({
        numberOfBookings: 0,
        revenue: 0,
    });
    React.useEffect(() => {
        let bookings = 0;
        let revenue = 0;

        bookingSearch?.forEach((booking) => {
            bookings += 1;
            revenue += booking.total;
        });
        setBookingSummary({
            numberOfBookings: bookings,
            revenue,
        });
    }, [bookingSearch]);

    function searchInList(list: any[], searchText: string) {
        let searchResults = list.filter(function (element) {
            return element.user.displayName.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
        });
        setBookingSearch(searchResults);
    }
    React.useEffect(() => {
        searchInList(bookings, search);
    }, [search]);
    return (
        <Layout>
            <TopNav
                middleContent={`Bookings for ${name}`}
                leftContent={<Ionicons name="chevron-back" size={20} />}
                leftAction={() => navigation.goBack()}
            />
            <View>
                <Input
                    containerStyle={{}}
                    disabledInputStyle={{ backgroundColor: '#ddd' }}
                    inputContainerStyle={{}}
                    errorStyle={{
                        margin: 0,
                    }}
                    value={search}
                    errorProps={{}}
                    inputStyle={{}}
                    labelStyle={{}}
                    labelProps={{}}
                    leftIcon={
                        <Button
                            type="clear"
                            icon={<Ionicons name="filter" color="#007bff" size={20} />}
                            onPress={() => setShowFilter(true)}
                        />
                    }
                    onChangeText={(val) => setSearch(val)}
                    leftIconContainerStyle={{}}
                    rightIcon={<Icon name="close" size={20} onPress={() => setSearch('')} />}
                    rightIconContainerStyle={{}}
                    placeholder="Search driver"
                    style={{}}
                />
                {filter && !showFilter ? (
                    <View style={{ flex: 1, padding: 12, flexDirection: 'row' }}>
                        <Button
                            title={'Clear filters'}
                            icon={
                                <Ionicons
                                    name="remove-circle"
                                    style={{ marginLeft: 6 }}
                                    size={24}
                                    color="red"
                                />
                            }
                            iconPosition="right"
                            buttonStyle={{
                                marginRight: 6,
                                maxWidth: 150,
                            }}
                            onPress={() => {
                                setFilter(false);
                                setBookingSearch(bookings);
                            }}
                        />
                    </View>
                ) : null}
                {showFilter && (
                    <DateFilter
                        onApply={onApply}
                        month={month}
                        year={year}
                        onYearChange={onYearPress}
                        onMonthChange={onMonthPress}
                        onDateChange={onDatePress}
                        onCancel={handleCancelFilter}
                    />
                )}
            </View>
            <ScrollView>
                <View style={{ flex: 1, flexDirection: 'row', padding: 20 }}>
                    <View style={{ width: '50%' }}>
                        <Text
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            Total bookings
                        </Text>
                        <Text
                            h1
                            h1Style={{
                                textAlign: 'center',
                            }}
                        >
                            {bookingSummary.numberOfBookings}
                        </Text>
                    </View>
                    <View style={{ width: '50%' }}>
                        <Text
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            Total revenue
                        </Text>
                        <Text
                            h1
                            h1Style={{
                                textAlign: 'center',
                            }}
                        >
                            ₱{bookingSummary.revenue}
                        </Text>
                    </View>
                </View>
                {bookingSearch?.map((booking) => (
                    <ListItem key={booking.id} topDivider onPress={() => handlePress(booking)}>
                        <Badge
                            status={
                                booking.status === 'ongoing'
                                    ? 'warning'
                                    : booking.status === 'cancelled'
                                    ? 'error'
                                    : 'success'
                            }
                        />

                        <ListItem.Content>
                            <ListItem.Title>{booking.reservation.plateNumber}</ListItem.Title>
                            <ListItem.Subtitle>
                                Book date:{' '}
                                {moment(booking.reservation.startDate).format('MMM DD, YYYY')}
                            </ListItem.Subtitle>
                            <ListItem.Subtitle>Price: ₱{booking.total}</ListItem.Subtitle>
                            <ListItem.Subtitle>
                                Started: {moment.unix(booking.creation).format('hh:mm A')}
                            </ListItem.Subtitle>
                            {booking.status === 'done' && (
                                <ListItem.Subtitle>
                                    Ended: {moment.unix(booking.ended).format('hh:mm A')}
                                </ListItem.Subtitle>
                            )}
                            {userData && userData.role === 'admin' ? (
                                <ListItem.Subtitle
                                    onPress={(e) => handlePressUser(e, booking.user)}
                                >
                                    Booked By:{' '}
                                    <Text style={{ color: '#007bff', fontWeight: '700' }}>
                                        {booking.user.displayName}
                                    </Text>
                                </ListItem.Subtitle>
                            ) : null}
                        </ListItem.Content>
                    </ListItem>
                ))}
            </ScrollView>
            {!bookingSearch || bookingSearch.length === 0 ? (
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text h3>No booking</Text>
                </View>
            ) : null}
        </Layout>
    );
};
