import React from 'react';
import { View, ScrollView } from 'react-native';
import { MainStackParamList } from '../types/navigation';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { supabase } from '../initSupabase';
import { Icon } from 'react-native-elements';
import { FAB } from 'react-native-elements';
import {
    Layout,
    Button,
    Text,
    TopNav,
    Section,
    SectionContent,
    useTheme,
    themeColor,
} from 'react-native-rapi-ui';
import { Ionicons } from '@expo/vector-icons';
import ParkingList from '../components/ParkingList';
import { useAuth } from '../provider/AuthProvider';

export default function ({
    navigation,
}: NativeStackScreenProps<MainStackParamList, 'ParkingLotsScreen'>) {
    const { isDarkmode, setTheme } = useTheme();

    const { userData } = useAuth();
    console.log(userData);
    return (
        <Layout>
            <TopNav
                middleContent="Home"
                rightContent={
                    <Ionicons
                        name={'ios-log-out-outline'}
                        size={20}
                        // color={isDarkmode ? themeColor.white100 : themeColor.dark}
                    />
                }
                leftContent={
                    userData && userData.role !== 'driver' ? (
                        <Ionicons
                            name={'add'}
                            size={20}
                            // color={isDarkmode ? themeColor.white100 : themeColor.dark}
                        />
                    ) : null
                }
                leftAction={() => {
                    if (!userData || userData.role === 'driver') return;
                    navigation.navigate('AddParkingLotScreen');
                }}
                rightAction={async () => {
                    const { error } = await supabase.auth.signOut();
                    if (!error) {
                        alert('Signed out!');
                    }
                    // if (isDarkmode) {
                    //   setTheme("light");
                    // } else {
                    //   setTheme("dark");
                    // }
                }}
            />
            <ParkingList />

            {userData && userData.role === 'admin' ? (
                <FAB
                    icon={<Ionicons name="add-circle" color="white" size={30} />}
                    color="#007bff"
                    placement="right"
                    title={'New'}
                    onPress={() => {
                        navigation.navigate('AddParkingLotScreen');
                    }}
                />
            ) : null}
        </Layout>
    );
}
