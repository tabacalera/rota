import { Ionicons } from '@expo/vector-icons';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { supabase } from '../../initSupabase';
import React from 'react';
import { Layout, themeColor, TopNav } from 'react-native-rapi-ui';
import { MainStackParamList } from '../../types/navigation';
import { Badge, ListItem } from 'react-native-elements';
import moment from 'moment';
import { ScrollView } from 'react-native';

const UserHistoryScreen = ({
    navigation,
    route,
}: NativeStackScreenProps<MainStackParamList, 'UserHistoryScreen'>) => {
    console.log(route.params);
    const params: any = route.params;
    const user = params?.user;
    const [bookings, setBookings] = React.useState<any[]>();

    const getBookingDataByUser = async () => {
        console.log(user.id);
        const { data, error }: any = await supabase.from('booking').select().match({
            bookedBy: user.id,
        }).select(`
              *,
              parking: parkingId(*),
              reservation: reservationId(*),
              user: bookedBy(*)
            `);
        console.log('useeeeeeeeer', data, error);
        if (error) {
            console.log(error);
            return;
        }
        console.log(data, error);
        setBookings(data);
    };

    React.useEffect(() => {
        if (!user) return;
        getBookingDataByUser();
    }, [user]);

    return (
        <Layout>
            <TopNav
                leftContent={<Ionicons name="chevron-back" size={20} color={themeColor.dark} />}
                leftAction={() => navigation.goBack()}
                middleContent={`${user.displayName}'s booking history`}
            />
            <ScrollView>
                {bookings?.map((booking) => (
                    <ListItem key={booking.id} topDivider>
                        <Badge
                            status={
                                booking.status === 'ongoing'
                                    ? 'warning'
                                    : booking.status === 'cancelled'
                                    ? 'error'
                                    : 'success'
                            }
                        />

                        <ListItem.Content>
                            <ListItem.Title>{booking.reservation.plateNumber}</ListItem.Title>
                            <ListItem.Subtitle>
                                Book date:{' '}
                                {moment(booking.reservation.startDate).format('MMM DD, YYYY')}
                            </ListItem.Subtitle>
                            <ListItem.Subtitle>Price: ₱{booking.total}</ListItem.Subtitle>
                            <ListItem.Subtitle>
                                Started: {moment.unix(booking.creation).format('hh:mm A')}
                            </ListItem.Subtitle>
                            {booking.status === 'done' && (
                                <ListItem.Subtitle>
                                    Ended: {moment.unix(booking.ended).format('hh:mm A')}
                                </ListItem.Subtitle>
                            )}
                        </ListItem.Content>
                    </ListItem>
                ))}
            </ScrollView>
        </Layout>
    );
};

export default UserHistoryScreen;
