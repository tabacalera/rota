import React from 'react';
import { Camera, Constants } from 'expo-camera';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { MainStackParamList } from '../../types/navigation';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { CameraType } from 'expo-camera/build/Camera.types';
import { Layout } from 'react-native-rapi-ui';
import { SafeAreaView } from 'react-native-safe-area-context';
import { supabase } from '../../initSupabase';
import { Ionicons } from '@expo/vector-icons';
import { BarCodeScanner } from 'expo-barcode-scanner';

const ScanReservationScreen = ({
    navigation,
    route,
}: NativeStackScreenProps<MainStackParamList, 'ScanReservationScreen'>) => {
    const [type, setType] = React.useState(CameraType.back);
    const [permission, setPermission] = React.useState<any>({
        status: '',
    });

    const onScan = async (scan: any) => {
        console.log(scan);
        if (!scan.data) return;
        const reservationId = scan.data;
        const { data, error } = await supabase.from('reservation').select().eq('id', reservationId);
        if (error) {
            return;
        }
        console.log(data);
        if (!data[0]) {
            return;
        }
        navigation.replace('AddReservationScreen', { reservation: data[0], isQr: true } as any);
    };

    React.useEffect(() => {
        const request = async () => {
            const resp = await Camera.requestCameraPermissionsAsync();
            setPermission(resp);
        };

        request();
    }, [permission.status]);

    return (
        <Layout>
            <View
                style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}
            >
                {permission.status === 'granted' && (
                    <BarCodeScanner
                        // autoFocus
                        // barCodeScannerSettings={{
                        //     barCodeTypes: [BarCodeScanner.Constants.BarCodeType],
                        // }}
                        onBarCodeScanned={onScan}
                        style={{
                            width: '100%',
                            height: '100%',
                        }}
                        type={type}
                        barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
                    ></BarCodeScanner>
                )}
            </View>
        </Layout>
    );
};

export default ScanReservationScreen;
