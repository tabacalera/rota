import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { useAuth } from '../../provider/AuthProvider';
import React from 'react';
import { RefreshControl, ScrollView, View } from 'react-native';
import { ListItem, Avatar, Button, Text, Badge } from 'react-native-elements';
import { Layout } from 'react-native-rapi-ui';
import { supabase } from '../../initSupabase';
import { Parking, ParkingReservation } from '../../types/parking';

const ReservationItem = ({
    reservation,
    handleDelete,
    parkingData,
}: {
    handleDelete: (reservationId: string) => void;
    reservation: ParkingReservation;
    parkingData?: Parking;
}) => {
    const navigation = useNavigation<any>();
    const deleteReservation = async (reservation: ParkingReservation) => {
        await supabase.from('reservation').delete().eq('id', reservation.id);
        handleDelete(reservation.id as string);
    };
    const { userData } = useAuth();

    const handlePress = () => {
        navigation.navigate('AddReservationScreen', { parkingData, reservation });
    };
    return (
        <ListItem key={reservation.id} topDivider onPress={handlePress}>
            <Avatar
                size={42}
                avatarStyle={{
                    borderColor:
                        reservation.status === 'pending'
                            ? 'yellow'
                            : reservation.status === 'approved'
                            ? 'green'
                            : 'red',
                    borderWidth: 2,
                }}
            />
            <ListItem.Content>
                <ListItem.Title>{reservation.plateNumber}</ListItem.Title>
                <ListItem.Subtitle>
                    {reservation.startDate} - {reservation.startTime}
                </ListItem.Subtitle>
            </ListItem.Content>

            {userData && userData.role !== 'driver' ? (
                <Button
                    type="clear"
                    onPress={() => deleteReservation(reservation)}
                    icon={<Ionicons name={'trash-bin'} size={18} color="red" />}
                />
            ) : null}
        </ListItem>
    );
};

const ReservationList = ({
    parkingData,
    routeKey,
}: {
    routeKey: any;
    parkingData: Parking | undefined;
}) => {
    const [reservations, setReservations] = React.useState<ParkingReservation[]>([]);
    const [isLoading, setIsLoading] = React.useState(false);
    const { userData } = useAuth();
    const handleDelete = async (id: string) => {
        const newReservations = [...reservations];
        const idx = newReservations.findIndex((parkRes) => parkRes.id === id);
        newReservations.splice(idx, 1);
        setReservations(newReservations);
    };

    const handleRefresh = () => {
        if (!userData) return;
        if (!userData.role) return;
        if (userData && userData.role === 'driver') {
            getUserReservations();
            return;
        }
        if (parkingData) {
            getParkingReservations();
            return;
        }

        getReservations();
    };
    const getReservations = async () => {
        const { data, error } = await supabase.from('reservation').select();
        console.log(data, error);
        if (error) return;
        setReservations(data);
    };
    const getUserReservations = async () => {
        const { data, error } = await supabase.from('reservation').select().match({
            reservedBy: userData?.id,
        });
        if (error) return;
        setReservations(data);
    };
    const getParkingReservations = async () => {
        const { data, error } = await supabase
            .from('reservation')
            .select()
            // @ts-ignore
            .match({
                reservedBy: userData?.id,
            })
            .match({
                parkingId: parkingData?.id,
            });
        if (error) return;
        setReservations(data);
    };

    React.useEffect(() => {
        if (!userData) return;
        if (!userData.role) return;
        if (userData && userData.role === 'driver') {
            getUserReservations();
            return;
        }
        if (parkingData) {
            getParkingReservations();
            return;
        }

        getReservations();
    }, [userData, parkingData]);

    return (
        <ScrollView
            refreshControl={<RefreshControl refreshing={isLoading} onRefresh={handleRefresh} />}
        >
            <View style={{ flex: 1, flexGrow: 1 }}>
                {reservations.map((res) => (
                    <ReservationItem
                        key={res.id}
                        reservation={res}
                        parkingData={parkingData}
                        handleDelete={handleDelete}
                    />
                ))}
            </View>
        </ScrollView>
    );
};

export default ReservationList;
