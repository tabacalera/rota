import { Ionicons } from '@expo/vector-icons';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React from 'react';
import * as DocumentPicker from 'expo-document-picker';
import { View, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { Form, FormItem, Label, Modal, Picker } from 'react-native-form-component';
import { Layout, TopNav } from 'react-native-rapi-ui';
import { supabase } from '../../initSupabase';
import { MainStackParamList } from '../../types/navigation';
import DateTimePicker, {
    WindowsDatePickerChangeEvent,
} from '@react-native-community/datetimepicker';
import {
    Text,
    Divider,
    ListItem,
    Avatar,
    Image,
    Card,
    Button,
    Dialog,
} from 'react-native-elements';
import moment from 'moment';
import QRCode from 'react-native-qrcode-svg';
import { getParking, getParkings } from '../../data/parking.actions';
import { updateReservation } from '../../data/reservation.action';
import { Parking, ParkingReservation } from '../../types/parking';
import { useAuth, UserData } from '../../provider/AuthProvider';
interface PickerItem {
    label: string;
    value: string | number;
}
const MyDatePicker = ({
    handleChange,
    onCancel,
}: {
    handleChange: (event: any, date?: Date) => void;
    onCancel: () => void;
}) => {
    return (
        <DateTimePicker
            style={{ width: 200 }}
            value={new Date()}
            mode="time"
            // placeholder="select date"
            // format="YYYY-MM-DD hh:mm"
            // confirmBtnText="Confirm"
            // cancelBtnText="Cancel"
            // onChange={handleChange}
            // onTouchCancel={onCancel}
        />
    );
};
const nameValue: {
    [key: string]: any;
} = {
    parkingId: 'Parking Space',
    startDate: 'Start Date',
    startTime: 'Start Time',
    duration: 'Duration',
    plateNumber: 'Plate Number',
    vehicleType: 'Vehicle Type',
};
const AddParkingReservation = ({
    navigation,
    route,
}: NativeStackScreenProps<MainStackParamList, 'AddReservationScreen'>) => {
    const [parkings, setParkings] = React.useState<Parking[]>([]);
    const [fileResponse, setFileResponse] = React.useState<any>([]);
    const { userData }: any = useAuth();
    const { role } = userData;
    const userRole: UserData['role'] = role || 'driver';
    const [showImage, setShowImage] = React.useState(false);
    const [showDate, setShowDate] = React.useState<{
        show: boolean;
        mode: 'date' | 'time';
    }>({
        show: false,
        mode: 'date',
    });
    const [isEdit, setIsEdit] = React.useState(false);
    const [isLoading, setIsLoading] = React.useState(false);
    const [isExpanded, setIsExpanded] = React.useState(true);
    const [fileExpanded, setFileExpanded] = React.useState(true);
    const [currentImage, setCurrentImage] = React.useState({
        uri: '',
        name: '',
    });
    const params: any = route.params;
    const parking = params.parkingData;
    const name = parking && parking.name ? parking.name : '';
    const [parkingData, setParkingData] = React.useState<any>(
        parking || {
            id: '',
        }
    );
    const [bookingData, setBookingData] = React.useState<any>();
    const [values, setValues] = React.useState<{
        [key: keyof typeof nameValue]: any;
    }>({
        parkingId: '',
        startDate: '',
        startTime: '',
        duration: '',
        plateNumber: '',
        files: [],
        bookingId: '',
        vehicleType: '',
    });

    const [error, setError] = React.useState<{
        [key: string]: any;
    }>({
        show: true,
        message: '',
    });
    const handleDocumentSelection = async () => {
        if (!isEdit || values.status !== 'pending') return;
        try {
            const response = await DocumentPicker.getDocumentAsync({
                multiple: true,
                type: ['image/*'],
            });
            if (response.type === 'success') {
                const newName = `public/${moment().format('MM-DD-YYYY-hh-mm-ss-a')}-${
                    response.name
                }`;
                const { data, error } = await supabase.storage
                    .from('payments')
                    .upload(newName, response as any, {
                        contentType: response.mimeType,
                    });
                if (error) {
                    setError({
                        show: true,
                        message: error.message,
                    });
                    return;
                }
                if (!data) return;
                const url = supabase.storage.from('payments').getPublicUrl(newName);
                const dataFile = {
                    uri: url.data?.publicURL,
                    name: newName,
                    path: data.Key,
                    creation: moment().unix(),
                    uploadedBy: supabase.auth.user()?.id,
                    reservationId: values.id,
                    parkingId: values.parkingId,
                };
                const resp = await supabase.from('files').insert(dataFile);
                if (resp.error) return;
                const newFiles = [...fileResponse, resp.data[0]];
                setFileResponse(newFiles);
            }
        } catch (err) {
            console.warn(err);
        }
    };
    const handleDeleteImage = async (img: any) => {
        const { error } = await supabase.from('files').delete().eq('id', img.id);
        if (!error) {
            setCurrentImage({
                uri: '',
                name: '',
            });
            setShowImage(false);
            const newFiles = [...fileResponse];
            newFiles.splice(img.fileIdx, 1);
            setFileResponse(newFiles);
        }
    };
    const onChange = (name: string, value: any) => {
        setValues({
            ...values,
            [name]: value,
        });
    };
    const onSubmit = async () => {
        const status = validate();
        if (status) return;

        const parking = await getParking(values.parkingId);
        if (!parking) {
            setError({
                show: true,
                message: "Can't find parking space. Try again or select another parking space.",
            });
            return;
        }
        if (parking && parking.availableSlots === 0) {
            setError({
                show: true,
                message: 'Parking space already full.',
            });
            return;
        }

        setError({
            show: false,
            message: '',
        });
        const payload: ParkingReservation = {
            creation: moment().unix(),
            reservedBy: supabase.auth.user()?.id as string,
            status: 'pending',
            duration: values.duration,
            startDate: values.startDate,
            startTime: values.startTime,
            plateNumber: values.plateNumber,
            parkingId: values.parkingId,
            vehicleType: values.vehicleType,
        };
        const { data, error } = await supabase.from('reservation').insert(payload);
        if (error) {
            setError({
                show: true,
                message: error.message,
            });
            return;
        }

        if (params && params.parkingData) {
            navigation.navigate('ParkingPageScreen', params.parkingData);
            return;
        }
        navigation.reset(navigation.getState());

        navigation.navigate('Reservation');
    };
    const onUpdate = async () => {
        const status = validate();
        if (status) return;
        // const payload =
        const resp = await updateReservation(values.id, {
            ...values,
        });

        console.log(resp);
    };
    const validate = () => {
        const err = {
            show: false,
            message: '',
        };

        const { bookingId, files, ...rest } = values;
        const keys = Object.keys(rest);
        keys.some((key: string) => {
            const value = values[key];
            if (!value) {
                err.show = true;
                err.message = `${nameValue[key]} is required.`;
                return true;
            }
        });
        setError(err);
        return err.show ? err.message : false;
    };

    const handleSelect = (val: PickerItem) => {
        const prkIdx = parkings.findIndex(
            (park: Parking) => park.id === val.value && park.name === val.label
        );
        const parkingData = parkings[prkIdx];
        if (parkingData) {
            setParkingData(parkingData);
            setValues({
                ...values,
                parkingId: parkingData.id,
            });
        }
    };

    function formatAMPM(date: Date) {
        let hours: any = date.getHours();
        let minutes: any = date.getMinutes();
        let ampm: any = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }
    const handleDateChange = (event: WindowsDatePickerChangeEvent, date?: Date) => {
        if (showDate.mode === 'date') {
            if (date) {
                setValues({
                    ...values,
                    startDate: moment(date?.toLocaleDateString()).format('MMM DD, YYYY'),
                });
            }
            setShowDate({
                show: false,
                mode: 'date',
            });
        }
        if (showDate.mode === 'time') {
            if (date) {
                setValues({
                    ...values,
                    startTime: formatAMPM(date),
                });
            }

            setShowDate({
                show: false,
                mode: 'date',
            });
        }
    };
    const selectStartDate = (mode: 'time' | 'date') => {
        setShowDate({ show: true, mode });
        // setOpen(true);
        // DateTimePickerAndroid.open({
        //     mode,
        //     value: new Date(),
        //     onChange(_event: any, date: any) {
        //         if (mode === 'date') {
        //             setValues({
        //                 ...values,
        //                 startDate: date?.toLocaleDateString(),
        //             });
        //         }
        //         if (mode === 'time') {
        //             setValues({
        //                 ...values,
        //                 startTime: date?.toLocaleTimeString(),
        //             });
        //         }
        //     },
        // });
    };

    React.useEffect(() => {
        if (!values.bookingId) return;

        const getBookingData = async () => {
            const { data, error }: any = await supabase
                .from('booking')
                .select()
                .eq('id', values.bookingId);
            if (error) {
                return;
            }
            setBookingData(data[0]);
        };

        getBookingData();
    }, [values.bookingId]);

    React.useEffect(() => {
        if (values.status === 'approved') {
            setIsExpanded(false);
            setFileExpanded(false);
        }
    }, [values.status]);

    React.useEffect(() => {
        if (!values.id) return;
        setIsEdit(true);
    }, [values.id]);

    React.useEffect(() => {
        if (params && params.reservation) {
            const getParking = async () => {
                setIsLoading(true);
                try {
                    const reservationFiles = await supabase
                        .from('reservation')
                        .select()
                        .eq('id', params.reservation.id);

                    if (reservationFiles.error) return;
                    const reservationData = reservationFiles.data[0];
                    setValues({
                        ...reservationData,
                        duration: `${reservationData.duration}`,
                        parkingId: `${reservationData.parkingId}`,
                    });
                    const respFiles = await supabase
                        .from('files')
                        .select()
                        .eq('reservationId', reservationData.id)
                        .eq('parkingId', reservationData.parkingId);
                    setFileResponse(respFiles.data);
                    const { data, error } = await supabase
                        .from('parking')
                        .select()
                        .eq('id', reservationData.parkingId);
                    if (error) return;
                    setParkings(data);
                    setParkingData(data[0]);
                    setIsLoading(false);
                } catch (err) {
                    setIsLoading(false);
                }
                setIsLoading(false);
            };

            getParking();
            return;
        }
        if (name) {
            setParkings([parkingData]);
            setValues({
                ...values,
                parkingId: parkingData.id,
            });
            return;
        }
        const getParkingsData = async () => {
            const parkings = await getParkings();
            setParkings(parkings);
        };

        getParkingsData();
    }, []);

    return (
        <Layout>
            <TopNav
                leftContent={
                    <Ionicons
                        name={'chevron-back'}
                        size={20}
                        // color={isDarkmode ? themeColor.white100 : themeColor.dark}
                    />
                }
                leftAction={() => {
                    navigation.goBack();
                }}
                middleContent={
                    values.status === 'approved' && values.bookingId
                        ? 'Update Booking'
                        : isEdit
                        ? `Update reservation`
                        : name
                        ? `New reservation for ${name}`
                        : 'New reservation'
                }
                rightContent={
                    isEdit && values.status === 'pending' ? (
                        <Ionicons
                            name={'attach'}
                            size={24}
                            // color={isDarkmode ? themeColor.white100 : themeColor.dark}
                        />
                    ) : null
                }
                rightAction={handleDocumentSelection}
            />
            <ScrollView>
                <SafeAreaView>
                    <View>
                        {showDate.show && (
                            <DateTimePicker
                                style={{ width: 200 }}
                                value={new Date()}
                                is24Hour={false}
                                mode={showDate.mode}
                                // placeholder="select date"
                                format={'YYYY-MM-DD h:mm A'}
                                // confirmBtnText="Confirm"
                                // cancelBtnText="Cancel"
                                // @ts-ignore
                                onChange={handleDateChange}
                                // onTouchCancel={() =>
                                //     setShowDate({
                                //         show: false,
                                //         mode: '',
                                //     })
                                // }
                            />
                        )}
                        <View
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'flex-start',
                                backgroundColor: '#fff',
                                marginTop: 20,
                                padding: 5,
                            }}
                        >
                            <Modal show={isLoading} visible={isLoading}>
                                <View
                                    style={{
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        height: '100%',
                                        width: '100%',
                                        backgroundColor: 'rgba(0,0,0,0.3)',
                                    }}
                                >
                                    <Dialog.Loading />
                                </View>
                            </Modal>
                            <Modal
                                show={showImage}
                                visible={showImage}
                                onRequestClose={() => {
                                    setCurrentImage({
                                        uri: '',
                                        name: '',
                                    });
                                    setShowImage(false);
                                }}
                            >
                                <View
                                    style={{
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        height: '100%',
                                        width: '100%',
                                        backgroundColor: 'rgba(0,0,0,0.3)',
                                    }}
                                >
                                    <Card
                                        wrapperStyle={{
                                            height: '80%',
                                            width: '90%',
                                        }}
                                        containerStyle={{
                                            height: '80%',
                                            width: '90%',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <Card.Title>{currentImage.name}</Card.Title>
                                        <Card.Divider />

                                        {values.status === 'approved' &&
                                        userRole === 'driver' ? null : (
                                            <Button
                                                icon={
                                                    <Ionicons
                                                        name="trash-bin"
                                                        color="#fff"
                                                        size={18}
                                                        style={{ marginRight: 10 }}
                                                    />
                                                }
                                                title="Remove Image"
                                                buttonStyle={{
                                                    backgroundColor: 'red',
                                                    marginBottom: 10,
                                                }}
                                                onPress={() => handleDeleteImage(currentImage)}
                                            />
                                        )}
                                        <View>
                                            <Card.Image
                                                source={{
                                                    uri: currentImage.uri,
                                                }}
                                                style={{
                                                    width: '100%',
                                                    height: '100%',
                                                }}
                                                resizeMethod="auto"
                                                resizeMode="center"
                                            />
                                        </View>
                                    </Card>
                                </View>
                            </Modal>
                            {isEdit && (
                                <View
                                    style={{
                                        paddingTop: 10,
                                        paddingBottom: 10,
                                        alignItems: 'center',
                                    }}
                                >
                                    {values.status === 'approved' && (
                                        <QRCode value={values.id} size={150} />
                                    )}

                                    {values.status !== 'pending' && (
                                        <Text
                                            h2
                                            style={{
                                                color:
                                                    values.status === 'approved' ? 'green' : 'red',
                                                textTransform: 'capitalize',
                                            }}
                                        >
                                            {values.status}
                                        </Text>
                                    )}
                                    {values.status === 'pending' && (
                                        <Text
                                            h2
                                            style={{
                                                color: 'orange',
                                                textTransform: 'capitalize',
                                            }}
                                        >
                                            For Approval
                                        </Text>
                                    )}
                                </View>
                            )}
                        </View>
                        {isEdit && (
                            <View
                                style={{
                                    padding: 12,
                                    flex: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                {values.status === 'pending' &&
                                userData &&
                                userData.role === 'admin' ? (
                                    <>
                                        <Button
                                            title="Accept"
                                            buttonStyle={{ backgroundColor: 'green', width: 200 }}
                                            containerStyle={{ marginBottom: 10 }}
                                            onPress={async () => {
                                                await updateReservation(values.id, {
                                                    status: 'approved',
                                                });
                                                await supabase.rpc('increment_available_slots', {
                                                    x: -1,
                                                    row_id: parseInt(values.parkingId),
                                                });
                                                const booking = {
                                                    reservationId: values.id,
                                                    parkingId: parseInt(values.parkingId),
                                                    bookedBy: supabase.auth.user()?.id,
                                                    creation: moment().unix(),
                                                    status: 'ongoing',
                                                    total:
                                                        parseFloat(values.duration) *
                                                        parseFloat(parkingData.pricePerHour),
                                                };
                                                const { data, error }: any = await supabase
                                                    .from('booking')
                                                    .insert(booking)
                                                    .select();
                                                if (error) {
                                                    return;
                                                }
                                                const bookingData = data[0];
                                                await updateReservation(values.id, {
                                                    bookingId: bookingData.id,
                                                });
                                                setValues({
                                                    ...values,
                                                    status: 'approved',
                                                    bookingId: bookingData.id,
                                                });
                                            }}
                                        />
                                        <Button
                                            title="Reject"
                                            buttonStyle={{ backgroundColor: 'red', width: 200 }}
                                            onPress={async () => {
                                                await updateReservation(values.id, {
                                                    status: 'rejected',
                                                });

                                                setValues({
                                                    status: 'rejected',
                                                });
                                            }}
                                        />
                                    </>
                                ) : null}
                            </View>
                        )}
                        {isEdit && (
                            <ListItem.Accordion
                                isExpanded={fileExpanded}
                                onPress={() => {
                                    setFileExpanded((v: any) => !v);
                                }}
                                content={
                                    <>
                                        <ListItem.Content>
                                            <ListItem.Title>
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        marginTop: 10,
                                                        marginBottom: 10,
                                                    }}
                                                >
                                                    <Divider style={{ width: 100 }} />
                                                    <Ionicons name={'attach'} size={16} />
                                                    <Label
                                                        text={'Proof of payment'}
                                                        textStyle={{ color: '#bdbdbd' }}
                                                    />
                                                    <Divider style={{ width: 100 }} />
                                                </View>
                                            </ListItem.Title>
                                        </ListItem.Content>
                                    </>
                                }
                            >
                                <ListItem>
                                    <ListItem.Content>
                                        <View
                                            style={{
                                                flex: 1,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                flexDirection: 'row',
                                            }}
                                        >
                                            {fileResponse.map((file: any, idx: number) => (
                                                <Image
                                                    key={`${file.name}-${file.uri}`}
                                                    source={{
                                                        uri: file.uri,
                                                    }}
                                                    style={{
                                                        width: 100,
                                                        height: 100,
                                                    }}
                                                    resizeMode="contain"
                                                    onPress={() => {
                                                        setShowImage(true);
                                                        file.fileIdx = idx;
                                                        setCurrentImage(file);
                                                    }}
                                                />
                                            ))}
                                        </View>
                                    </ListItem.Content>
                                </ListItem>
                            </ListItem.Accordion>
                        )}
                        <ListItem.Accordion
                            isExpanded={isExpanded}
                            noIcon={!isEdit}
                            onPress={() => {
                                if (!isEdit) return;
                                setIsExpanded((v: any) => !v);
                            }}
                            content={
                                <>
                                    <ListItem.Content>
                                        <ListItem.Title>
                                            <View
                                                style={{
                                                    flex: 1,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    marginTop: 10,
                                                    marginBottom: 10,
                                                }}
                                            >
                                                <Divider style={{ width: 100 }} />
                                                <Label
                                                    text={'Reservation Info'}
                                                    textStyle={{ color: '#bdbdbd' }}
                                                />
                                                <Divider style={{ width: 100 }} />
                                            </View>
                                        </ListItem.Title>
                                    </ListItem.Content>
                                </>
                            }
                        >
                            <ListItem>
                                <ListItem.Content>
                                    <View
                                        style={{
                                            flex: 1,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}
                                        pointerEvents={
                                            values.status === 'approved' ||
                                            values.status === 'rejected'
                                                ? 'none'
                                                : 'auto'
                                        }
                                    >
                                        <Form
                                            buttonText={
                                                isEdit && values.id ? 'Update' : 'Reserve now'
                                            }
                                            buttonStyle={{
                                                backgroundColor:
                                                    values.status === 'approved' ||
                                                    values.status === 'rejected'
                                                        ? 'gray'
                                                        : '#007bff',
                                                marginTop: 5,
                                            }}
                                            style={{
                                                width: '100%',
                                                paddingTop: 0,
                                                marginTop: isEdit ? -24 : 0,
                                            }}
                                            onButtonPress={
                                                isEdit && values.id ? onUpdate : onSubmit
                                            }
                                        >
                                            <View style={{ paddingBottom: 0, marginBottom: -3 }}>
                                                <Picker
                                                    onSelection={handleSelect}
                                                    selectedValue={parkingData.id}
                                                    items={parkings
                                                        .filter(
                                                            (parking: Parking) =>
                                                                parking.availableSlots >= 1
                                                        )
                                                        .map((parking: Parking) => ({
                                                            label: parking.name,
                                                            value: parking.id as number,
                                                        }))}
                                                    buttonStyle={{
                                                        borderWidth: 1,
                                                    }}
                                                    label={'Parking Space'}
                                                    labelStyle={{
                                                        paddingBottom: 5,
                                                    }}
                                                />
                                            </View>
                                            <View
                                                style={{
                                                    flex: 1,
                                                    flexDirection: 'row',
                                                }}
                                            >
                                                <View
                                                    style={{
                                                        width: '50%',
                                                        marginRight: 5,
                                                    }}
                                                >
                                                    <FormItem
                                                        style={{
                                                            ...styles.input,
                                                            width: '100%',
                                                        }}
                                                        editable={values.status !== 'approved'}
                                                        label="Start Date"
                                                        value={values.startDate}
                                                        onChangeText={(val: string) =>
                                                            setValues({
                                                                ...values,
                                                                startDate: val,
                                                            })
                                                        }
                                                        showErrorIcon={false}
                                                        placeholder="Start Date"
                                                        onPressIn={() => selectStartDate('date')}
                                                    />
                                                </View>
                                                <View
                                                    style={{
                                                        width: '48%',
                                                        marginRight: 5,
                                                    }}
                                                >
                                                    <FormItem
                                                        style={{
                                                            ...styles.input,
                                                            width: '100%',
                                                        }}
                                                        label="Start Time"
                                                        editable={values.status !== 'approved'}
                                                        value={values.startTime}
                                                        onChangeText={(val: string) =>
                                                            setValues({
                                                                ...values,
                                                                startTime: val,
                                                            })
                                                        }
                                                        showErrorIcon={false}
                                                        placeholder="Start Time"
                                                        onPressIn={() => selectStartDate('time')}
                                                    />
                                                </View>
                                            </View>
                                            <FormItem
                                                isRequired
                                                style={styles.input}
                                                label="Duration in hours"
                                                value={values.duration}
                                                editable={values.status !== 'approved'}
                                                showErrorIcon={false}
                                                placeholder="Number of hours"
                                                onChangeText={(txt: any) => {
                                                    // const val = txt.replace(/[^0-9.]/g, '');
                                                    onChange('duration', txt);
                                                }}
                                            />
                                            <View
                                                style={{
                                                    flex: 1,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    marginTop: 10,
                                                    marginBottom: 10,
                                                }}
                                            >
                                                <Divider style={{ width: 110 }} />
                                                <Label
                                                    text={'Vehicle Details'}
                                                    textStyle={{ color: '#bdbdbd' }}
                                                />
                                                <Divider style={{ flex: 1 }} />
                                            </View>
                                            <FormItem
                                                isRequired
                                                style={styles.input}
                                                onChangeText={(txt: any) => {
                                                    const val = txt.replace(/[^a-zA-Z0-9\-]/g, '');
                                                    onChange('plateNumber', val);
                                                }}
                                                label="Plate Number"
                                                editable={values.status !== 'approved'}
                                                value={values.plateNumber}
                                                showErrorIcon={false}
                                                keyboardType="name-phone-pad"
                                            />
                                            <FormItem
                                                isRequired
                                                style={styles.input}
                                                onChangeText={(txt: any) => {
                                                    const val = txt.replace(/[^a-zA-Z0-9\-]/g, '');
                                                    onChange('vehicleType', val);
                                                }}
                                                label="Vehicle Type"
                                                editable={values.status !== 'approved'}
                                                value={values.vehicleType}
                                                showErrorIcon={false}
                                                keyboardType="name-phone-pad"
                                            />

                                            <View
                                                style={{
                                                    flex: 1,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    marginTop: 10,
                                                    marginBottom: 10,
                                                }}
                                            >
                                                <Divider style={{ width: 110 }} />
                                                <Label
                                                    text={'Pricing Detials'}
                                                    textStyle={{ color: '#bdbdbd' }}
                                                />
                                                <Divider style={{ flex: 1 }} />
                                            </View>

                                            <View
                                                style={{
                                                    flex: 1,
                                                    padding: 6,
                                                    backgroundColor: '#fafafa',
                                                }}
                                            >
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                        paddingBottom: 5,
                                                    }}
                                                >
                                                    <Text>Duration in hours</Text>
                                                    <Text>{values.duration}</Text>
                                                </View>
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                        paddingBottom: 5,
                                                    }}
                                                >
                                                    <Text>Price</Text>
                                                    <Text>₱{parkingData.pricePerHour}</Text>
                                                </View>
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Text style={styles.total}>Total</Text>
                                                    <Text style={styles.total}>
                                                        ₱
                                                        {parseFloat(values.duration) *
                                                            parseFloat(parkingData.pricePerHour)}
                                                    </Text>
                                                </View>
                                            </View>
                                            {error.show ? (
                                                <Label
                                                    text={error.message}
                                                    textStyle={{ color: 'red' }}
                                                />
                                            ) : null}
                                        </Form>
                                    </View>
                                </ListItem.Content>
                            </ListItem>
                        </ListItem.Accordion>
                    </View>
                    {values.status === 'approved' &&
                        values.bookingId &&
                        bookingData &&
                        bookingData.status === 'ongoing' && (
                            <View
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                            >
                                <Button
                                    title="End Now"
                                    titleStyle={{
                                        fontSize: 24,
                                    }}
                                    buttonStyle={{ height: 64, width: 300 }}
                                    onPress={async () => {
                                        await supabase.rpc('increment_available_slots', {
                                            x: 1,
                                            row_id: parseInt(values.parkingId),
                                        });
                                        const { data, error }: any = await supabase
                                            .from('booking')
                                            .update({
                                                status: 'done',
                                                ended: moment().unix(),
                                            })
                                            .eq('id', values.bookingId)
                                            .select();
                                        if (error) return;
                                        setBookingData(data[0]);
                                    }}
                                />
                            </View>
                        )}
                </SafeAreaView>
            </ScrollView>
        </Layout>
    );
};

export default AddParkingReservation;

const styles = StyleSheet.create({
    input: {
        height: 40,
        borderWidth: 1,
    },

    total: {
        fontSize: 18,
        fontWeight: '700',
    },
});
