import React from 'react';
import { RefreshControl, ScrollView, View } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { Layout, Text, TopNav } from 'react-native-rapi-ui';
import { MainStackParamList } from '../../types/navigation';
import { Ionicons } from '@expo/vector-icons';
import { supabase } from '../../initSupabase';
import { ParkingReservation } from '../../types/parking';
import ReservationList from './ReservationList';
import { Dialog } from 'react-native-elements';
import { Modal } from 'react-native-form-component';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useAuth } from '../../provider/AuthProvider';

export default function ({
    navigation,
    route,
}: NativeStackScreenProps<MainStackParamList, 'Reservation'>) {
    const parkingData: any =
        // @ts-ignore
        route.params && route.params.parkingData ? route.params.parkingData : undefined;

    const { userData } = useAuth();

    return (
        <Layout>
            <TopNav
                leftContent={<Ionicons name={'add'} size={20} />}
                leftAction={() => {
                    // @ts-ignore
                    navigation.navigate('AddReservationScreen', { parkingData });
                }}
                middleContent={
                    parkingData ? `Reservations for ${parkingData.name}` : 'Manage Reservations'
                }
                rightContent={
                    userData && userData.role === 'admin' ? (
                        <Ionicons name={'qr-code-outline'} size={20} />
                    ) : null
                }
                rightAction={() => {
                    if (userData && userData.role === 'driver') return;
                    navigation.navigate('ScanReservationScreen');
                }}
            />
            <ReservationList routeKey={route.key} key={route.key} parkingData={parkingData} />
        </Layout>
    );
}
