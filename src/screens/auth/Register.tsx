import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import {
    ScrollView,
    TouchableOpacity,
    View,
    KeyboardAvoidingView,
    Image,
    Alert,
} from 'react-native';
import { supabase } from '../../initSupabase';
import { AuthStackParamList } from '../../types/navigation';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { Layout, Text, TextInput, Button, useTheme, themeColor } from 'react-native-rapi-ui';
import moment from 'moment';

export default function ({ navigation }: NativeStackScreenProps<AuthStackParamList, 'Register'>) {
    const { isDarkmode, setTheme } = useTheme();
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [name, setName] = useState<string>('');
    const [phone, setPhone] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);

    async function register() {
        setLoading(true);
        const { user, error } = await supabase.auth.signUp({
            email: email,
            password: password,
        });
        if (!error) {
            setLoading(false);
            Alert.alert('Success!', 'Login now!');
            const userPayload = {
                id: user?.id,
                displayName: name,
                role: 'driver',
                creation: moment().unix(),
                email,
                phone,
            };
            const userResp = await supabase.from('userData').insert(userPayload);
            console.log(userResp);
        }
        if (error) {
            setLoading(false);
            alert(error.message);
        }
    }
    return (
        <KeyboardAvoidingView behavior="height" enabled style={{ flex: 1 }}>
            <Layout>
                <ScrollView
                    contentContainerStyle={{
                        flexGrow: 1,
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: isDarkmode ? '#17171E' : themeColor.white100,
                        }}
                    >
                        <Image
                            resizeMode="contain"
                            style={{
                                height: 220,
                                width: 220,
                            }}
                            source={require('../../../assets/images/register.png')}
                        />
                    </View>
                    <View
                        style={{
                            flex: 3,
                            paddingHorizontal: 20,
                            paddingBottom: 20,
                            backgroundColor: isDarkmode ? themeColor.dark : themeColor.white,
                        }}
                    >
                        <Text
                            fontWeight="bold"
                            size="h3"
                            style={{
                                alignSelf: 'center',
                                padding: 30,
                            }}
                        >
                            Register
                        </Text>
                        <Text>Name</Text>
                        <TextInput
                            containerStyle={{ marginTop: 15 }}
                            placeholder="Enter your name"
                            value={name}
                            autoCapitalize="words"
                            autoCorrect={false}
                            onChangeText={(text) => setName(text)}
                        />
                        <Text style={{ marginTop: 15 }}>Email</Text>
                        <TextInput
                            containerStyle={{ marginTop: 15 }}
                            placeholder="Enter your email"
                            value={email}
                            autoCapitalize="none"
                            autoCompleteType="off"
                            autoCorrect={false}
                            keyboardType="email-address"
                            onChangeText={(text) => setEmail(text)}
                        />
                        <Text style={{ marginTop: 15 }}>Phone Number</Text>
                        <TextInput
                            containerStyle={{ marginTop: 15 }}
                            placeholder="Enter your email"
                            value={phone}
                            autoCapitalize="none"
                            autoCompleteType="off"
                            autoCorrect={false}
                            keyboardType="phone-pad"
                            onChangeText={(text) => setPhone(text)}
                        />

                        <Text style={{ marginTop: 15 }}>Password</Text>
                        <TextInput
                            containerStyle={{ marginTop: 15 }}
                            placeholder="Enter your password"
                            value={password}
                            autoCapitalize="none"
                            autoCompleteType="off"
                            autoCorrect={false}
                            secureTextEntry={true}
                            onChangeText={(text) => setPassword(text)}
                        />
                        <Button
                            text={loading ? 'Loading' : 'Create an account'}
                            onPress={() => {
                                register();
                            }}
                            style={{
                                marginTop: 20,
                            }}
                            disabled={loading}
                        />

                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 15,
                                justifyContent: 'center',
                            }}
                        >
                            <Text size="md">Already have an account?</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    navigation.navigate('Login');
                                }}
                            >
                                <Text
                                    size="md"
                                    fontWeight="bold"
                                    style={{
                                        marginLeft: 5,
                                    }}
                                >
                                    Login heres
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 30,
                                justifyContent: 'center',
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    isDarkmode ? setTheme('light') : setTheme('dark');
                                }}
                            >
                                <Text
                                    size="md"
                                    fontWeight="bold"
                                    style={{
                                        marginLeft: 5,
                                    }}
                                >
                                    {isDarkmode ? '☀️ light theme' : '🌑 dark theme'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </Layout>
        </KeyboardAvoidingView>
    );
}
