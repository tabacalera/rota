export type MainStackParamList = {
    ParkingLotsScreen: undefined;
    AddParkingLotScreen: undefined;
    Reservation: undefined;
    BookingListScreen: undefined;
    AddReservationScreen: undefined;
    ParkingPageScreen: undefined;
    ScanReservationScreen: undefined;
    UserHistoryScreen: undefined;
};

export type AuthStackParamList = {
    Login: undefined;
    Register: undefined;
    ForgetPassword: undefined;
};

export type MainTabsParamList = {
    Home: undefined;
    Profile: undefined;
    About: undefined;
};
