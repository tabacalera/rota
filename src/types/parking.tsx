export interface Parking {
    id?: number;
    name: string;
    address: string;
    pricePerHour: number;
    numberOfSlots: number;
    creation: number;
    created_at?: string;
    availableSlots: number;
}

export interface ParkingReservation {
    id?: string;
    created_at?: string;
    creation: number;
    duration: number;
    startDate: string;
    startTime: string;
    plateNumber: string;
    parkingId: Parking['id'];
    reservedBy: string;
    status: 'pending' | 'approved' | 'rejected' | 'cancelled';
    files?: {
        name: string;
        uri: string;
    }[];
    bookingId?: string;
    vehicleType?: string;
}
