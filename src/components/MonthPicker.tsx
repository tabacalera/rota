import moment from 'moment';
import React from 'react';
import { Picker } from 'react-native-rapi-ui';

const MonthPicker = ({
    value,
    onChange,
    ...rest
}: {
    value: string | undefined;
    borderColor: string | undefined;

    onChange: (val: string) => void;
}) => {
    const [val, setVal] = React.useState<string | undefined>(value);
    const items: any = moment.months().map((month, idx) => ({
        label: month,
        value: idx,
    }));

    const handleChange = (val: string) => {
        setVal(val);
        onChange(val);
    };
    React.useEffect(() => {
        setVal(value);
    }, [value]);
    return (
        <Picker
            value={val}
            items={[{ label: 'None', value: undefined }, ...items]}
            onValueChange={handleChange}
            placeholder="Month"
            {...rest}
        />
    );
};

export default MonthPicker;
