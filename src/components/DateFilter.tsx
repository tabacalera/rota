import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { View } from 'react-native';
import { BottomSheet, Button, ListItem, Text } from 'react-native-elements';
import { TextInput } from 'react-native-rapi-ui';
import MonthPicker from './MonthPicker';
import YearPicker from './YearPicker';

interface DateFilerProps {
    onApply: () => Promise<boolean>;
    onCancel: () => void;
    onMonthChange: (month: string) => void;
    onYearChange: (year: string) => void;
    onDateChange: (date: string) => void;
    month: string | undefined;
    year: string | undefined;
}
const DateFilter = ({
    month,
    year,
    onCancel,
    onMonthChange,
    onDateChange,
    onYearChange,
    onApply,
}: DateFilerProps) => {
    const [err, setErr] = React.useState<any>(null);
    const handleApplyFilter = async () => {
        const invalid = await onApply();
        if (invalid) {
            setErr(
                <Text style={{ color: 'red', fontWeight: '100' }}>
                    Error date combination. Either:{'\n'}
                    <Text style={{ fontSize: 14, color: 'red', fontWeight: '700' }}>
                        Year
                    </Text>,{' '}
                    <Text style={{ fontSize: 14, color: 'red', fontWeight: '700' }}>
                        Year-Month
                    </Text>
                    , or{' '}
                    <Text style={{ fontSize: 14, color: 'red', fontWeight: '700' }}>
                        Year-Month-Date
                    </Text>{' '}
                    combinations are accepted!
                </Text>
            );
            return;
        }

        setErr('');
    };
    return (
        <BottomSheet isVisible>
            <ListItem topDivider>
                <ListItem.Content>
                    <ListItem.Title>Year</ListItem.Title>
                    <ListItem.Subtitle
                        style={{
                            width: 250,
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            alignContent: 'center',
                        }}
                    >
                        <View
                            style={{
                                width: 250,
                                flex: 1,
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                alignContent: 'center',
                            }}
                        >
                            <View
                                style={{
                                    width: 250,
                                }}
                            >
                                <YearPicker
                                    borderColor={err ? 'red' : undefined}
                                    key="notdefined"
                                    value={year}
                                    onChange={onYearChange}
                                />
                            </View>
                        </View>
                    </ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            <ListItem topDivider>
                <ListItem.Content>
                    <ListItem.Title>
                        Month (
                        <Text
                            h4
                            h4Style={{
                                color: '#a3a3a3',
                                fontWeight: '100',
                                fontSize: 16,
                            }}
                        >
                            Select none for year filter
                        </Text>
                        )
                    </ListItem.Title>
                    <ListItem.Subtitle
                        style={{
                            width: 250,
                            flex: 1,
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            alignContent: 'center',
                        }}
                    >
                        <View
                            style={{
                                width: 250,
                                flex: 1,
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                alignContent: 'center',
                            }}
                        >
                            <View
                                style={{
                                    width: 250,
                                }}
                            >
                                <MonthPicker
                                    borderColor={err ? 'red' : undefined}
                                    value={month}
                                    onChange={onMonthChange}
                                />
                            </View>
                        </View>
                    </ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            <ListItem topDivider>
                <ListItem.Content>
                    <ListItem.Title>
                        Date(
                        <Text
                            h4
                            h4Style={{
                                color: '#a3a3a3',
                                fontWeight: '100',
                                fontSize: 16,
                            }}
                        >
                            Leave blank for year-month filter
                        </Text>
                        )
                    </ListItem.Title>
                    <ListItem.Subtitle style={{ width: 250 }}>
                        <TextInput
                            containerStyle={{ width: 250 }}
                            style={{ width: 250 }}
                            placeholder="Date"
                            keyboardType="phone-pad"
                            borderColor={err ? 'red' : undefined}
                            onChangeText={onDateChange}
                        />
                    </ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            {err ? (
                <ListItem>
                    <ListItem.Title style={{ textAlign: 'center', width: '100%' }}>
                        {err}
                    </ListItem.Title>
                </ListItem>
            ) : null}
            <ListItem style={{ justifyContent: 'center' }}>
                <ListItem.Content>
                    <Button
                        onPress={onCancel}
                        title="Cancel"
                        type="clear"
                        containerStyle={{ width: '90%' }}
                    />
                </ListItem.Content>
                <ListItem.Content>
                    <Button
                        title="Apply"
                        onPress={handleApplyFilter}
                        containerStyle={{ width: '90%' }}
                    />
                </ListItem.Content>
            </ListItem>
        </BottomSheet>
    );
};

export default DateFilter;
