import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { useAuth } from '../provider/AuthProvider';
import React from 'react';
import { ListItem, Avatar, Badge, Button, Text, Card, Image, Input } from 'react-native-elements';
import { getParkings } from '../data/parking.actions';
import { supabase } from '../initSupabase';
import { Parking } from '../types/parking';
import { Menu, MenuTrigger, MenuOptions, MenuOption } from 'react-native-popup-menu';

import { ScrollView, RefreshControl, View } from 'react-native';
import { TextInput } from 'react-native-rapi-ui';
import { Modal } from 'react-native-form-component';

const ParkingItem = ({
    parkingLot,
    handleDelete,
}: {
    handleDelete: (parkId: number) => void;
    parkingLot: Parking;
}) => {
    const navigation = useNavigation();
    const [showImage, setShowImage] = React.useState(false);
    const [fileResponse, setFileResponse] = React.useState<any>([]);
    const { userData } = useAuth();
    const deleteParking = async () => {
        console.log(parkingLot);
        await supabase.from('parking').delete().eq('id', parkingLot.id);
        handleDelete(parkingLot.id as number);
    };

    const handlePress = () => {
        // @ts-ignore
        navigation.navigate('ParkingPageScreen', { ...parkingLot, parkingId: parkingLot.id });
    };

    const updateParking = () => {
        console.log(parkingLot);
        // @ts-ignore
        navigation.navigate('AddParkingLotScreen', { ...parkingLot, parkingId: parkingLot.id });
    };

    const handleShowImages = async () => {
        setShowImage(true);
        const respFiles = await supabase
            .from('parkingFiles')
            .select()
            .eq('parkingId', parkingLot.id);
        console.log(respFiles);
        setFileResponse(respFiles.data);
    };
    console.log(userData, fileResponse);
    return (
        <ListItem key={parkingLot.id} topDivider>
            <Avatar
                size={42}
                avatarStyle={{
                    borderColor: '#f3f3f3',
                    borderWidth: 1,
                }}
                renderPlaceholderContent={
                    <Text style={{ fontSize: 10 }}>
                        {parkingLot.numberOfSlots - parkingLot.availableSlots} /{' '}
                        {parkingLot.numberOfSlots}
                    </Text>
                }
            />
            <Modal show={showImage} visible={showImage}>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: '100%',
                        width: '100%',
                        backgroundColor: 'rgba(0,0,0,0.3)',
                    }}
                >
                    <Card
                        wrapperStyle={{
                            height: '100%',
                            width: '100%',
                        }}
                        containerStyle={{
                            height: '80%',
                            width: '90%',
                            alignItems: 'center',
                        }}
                    >
                        <Card.Title>
                            <Ionicons
                                name={'md-close-sharp'}
                                size={36}
                                onPress={() => {
                                    setShowImage(false);
                                }}
                            />
                        </Card.Title>
                        <Card.Divider />
                        <ScrollView style={{ height: '100%' }}>
                            <View style={{ height: '100%' }}>
                                {fileResponse.map((file: any, idx: number) => (
                                    <Image
                                        key={`${file.name}-${file.uri}`}
                                        source={{
                                            uri: file.uri,
                                        }}
                                        style={{
                                            width: 300,
                                            height: 300,
                                            maxWidth: '100%',
                                            marginBottom: 15,
                                        }}
                                        resizeMode="contain"
                                    />
                                ))}
                            </View>
                        </ScrollView>
                    </Card>
                </View>
            </Modal>
            <ListItem.Content>
                <ListItem.Title>{parkingLot.name}</ListItem.Title>
                <ListItem.Subtitle>
                    <Text
                        style={{
                            fontWeight: '100',
                            color: '#007bff',
                        }}
                        onPress={handleShowImages}
                    >
                        {parkingLot.address}
                    </Text>
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                    ₱{parkingLot.pricePerHour}
                    <Text style={{ fontSize: 11 }}> / hr</Text>
                </ListItem.Subtitle>
            </ListItem.Content>

            {/* {userData && userData.role === 'admin' && (
                <Button
                    type="clear"
                    onPress={() => deleteParking(parkingLot)}
                    icon={<Ionicons name={'pencil-outline'} size={18} color="red" />}
                />
            )} */}
            {/* <ListItem.Chevron
                iconProps={{
                    name: 'dots-three-vertical',
                }}
            /> */}
            <Menu>
                <MenuTrigger>
                    <Text style={{ color: '#007bff' }}>More</Text>
                </MenuTrigger>

                <MenuOptions
                    optionsContainerStyle={{
                        paddingBottom: 12,
                        borderWidth: 0,
                    }}
                >
                    <Card
                        containerStyle={{ borderWidth: 0, padding: 0, shadowColor: 'transparent' }}
                        wrapperStyle={{
                            padding: 0,
                            margin: 0,
                        }}
                    >
                        <MenuOption
                            style={{
                                borderBottomColor: '#f3f3f3',
                                borderBottomWidth: 1,
                                paddingTop: 12,
                                paddingBottom: 12,
                            }}
                            onSelect={handlePress}
                            text="View"
                        />
                        {userData && userData.role === 'admin' && (
                            <MenuOption
                                style={{
                                    borderBottomColor: '#f3f3f3',
                                    borderBottomWidth: 1,
                                    paddingTop: 12,
                                    paddingBottom: 12,
                                }}
                                onSelect={updateParking}
                                text="Update"
                            />
                        )}
                        {userData && userData.role === 'admin' && (
                            <MenuOption
                                style={{
                                    paddingTop: 12,
                                    paddingBottom: 12,
                                }}
                                onSelect={() => deleteParking()}
                            >
                                <Text style={{ color: 'red' }}>Delete</Text>
                            </MenuOption>
                        )}
                    </Card>
                </MenuOptions>
            </Menu>

            {/* {userData && userData.role === 'admin' && (
                <Button
                    type="clear"
                    onPress={() => deleteParking(parkingLot)}
                    icon={<Ionicons name={'trash-bin'} size={18} color="red" />}
                />
            )} */}
        </ListItem>
    );
};

const ParkingList = ({ navigation, route }: any) => {
    const [parkings, setParkings] = React.useState<Parking[]>([]);
    const [searchParkings, setSearchParkings] = React.useState<Parking[]>([]);
    const [isLoading, setIsLoading] = React.useState(false);
    const [search, setSearch] = React.useState('');

    const handleDelete = async (id: number) => {
        const newParkings = [...parkings];
        const idx = newParkings.findIndex((park) => park.id === id);
        newParkings.splice(idx, 1);
        setParkings(newParkings);
    };

    const getParkingsData = async () => {
        const parkings = await getParkings();
        setParkings(parkings);
        setSearchParkings(parkings);
    };
    function searchInList(list: Parking[], searchText: string) {
        let searchResults = list.filter(function (element) {
            return element.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
        });
        setSearchParkings(searchResults);
    }

    React.useEffect(() => {
        // const interval = setInterval(() => {
        getParkingsData();
        // console.log('wah')
        // }, 5000);
        // return () => clearInterval(interval);
    }, [route]);
    React.useEffect(() => {
        searchInList(parkings, search);
    }, [search]);
    return (
        <ScrollView
            refreshControl={
                <RefreshControl
                    refreshing={isLoading}
                    onRefresh={() => {
                        setIsLoading(true);
                        getParkingsData;
                        setIsLoading(false);
                    }}
                />
            }
        >
            <View style={{ flex: 1, flexGrow: 1 }}>
                <TextInput
                    placeholder="Search"
                    value={search}
                    onChangeText={(val) => setSearch(val)}
                    leftContent={<Ionicons name="search" size={20} />}
                    rightContent={
                        search ? (
                            <Ionicons
                                onPress={() => {
                                    setSearchParkings(parkings);
                                    setSearch('');
                                }}
                                name={'md-close-sharp'}
                                size={18}
                            />
                        ) : null
                    }
                    containerStyle={{
                        borderRadius: 0,
                        borderBottomEndRadius: 0,
                        borderBottomLeftRadius: 0,
                        borderBottomRightRadius: 0,
                        borderBottomStartRadius: 0,
                    }}
                    style={{
                        borderRadius: 0,
                        borderBottomEndRadius: 0,
                        borderBottomLeftRadius: 0,
                        borderBottomRightRadius: 0,
                        borderBottomStartRadius: 0,
                    }}
                />
            </View>

            {searchParkings.map((parkingLot) => (
                <ParkingItem
                    key={parkingLot.id}
                    handleDelete={handleDelete}
                    parkingLot={parkingLot}
                />
            ))}
        </ScrollView>
    );
};

export default ParkingList;
