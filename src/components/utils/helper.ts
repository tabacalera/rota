export const generateYear = () => {
    const startValue = 2022;
    const endValue = 2050;
    var result = [];
    for (var i = startValue; i <= endValue; i++) {
        result.push({ value: i.toString(), label: i.toString() });
    }
    return result;
};
