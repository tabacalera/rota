import moment from 'moment';
import React from 'react';
import { Picker } from 'react-native-rapi-ui';
import { generateYear } from './utils/helper';

const items: any = generateYear();
const YearPicker = ({
    value,
    onChange,
    ...rest
}: {
    value: string | undefined;
    borderColor: string | undefined;
    onChange: (val: string) => void;
}) => {
    const [val, setVal] = React.useState<string | undefined>(value);

    const handleChange = (val: string) => {
        setVal(val);
        onChange(val);
    };

    React.useEffect(() => {
        setVal(value);
    }, [value]);
    return (
        <Picker
            value={val}
            onValueChange={handleChange}
            items={[{ label: 'None', value: undefined }, ...items]}
            placeholder="Year"
            {...rest}
        />
    );
};

export default YearPicker;
