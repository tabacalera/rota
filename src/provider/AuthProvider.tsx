import React, { createContext, useState, useEffect } from 'react';
import { supabase } from '../initSupabase';
import { Session } from '@supabase/supabase-js';
type ContextProps = {
    user: null | boolean;
    session: Session | null;
    userData: {
        role: 'driver' | 'admin';
        id: string;
        displayName: string;
        creation: number;
        email: string;
    };
};

export type UserData = ContextProps['userData'];

const AuthContext = createContext<Partial<ContextProps>>({});

interface Props {
    children: React.ReactNode;
}

const AuthProvider = (props: Props) => {
    // user null = loading
    const [user, setUser] = useState<null | boolean>(null);
    const [session, setSession] = useState<Session | null>(null);
    const [userData, setUserData] = useState<ContextProps['userData']>({
        role: 'driver',
        id: '',
        displayName: '',
        creation: 0,
        email: '',
    });

    useEffect(() => {
        const session = supabase.auth.session();
        setSession(session);
        setUser(session ? true : false);
        const { data: authListener } = supabase.auth.onAuthStateChange(async (event, session) => {
            console.log(`Supabase auth event: ${event}`);
            setSession(session);
            setUser(session ? true : false);
        });
        return () => {
            authListener!.unsubscribe();
        };
    }, [user, userData]);

    useEffect(() => {
        if (!session) return;
        if (!session.user) return;

        const getUserData = async () => {
            const { data, error } = await supabase
                .from('userData')
                .select()
                .eq('id', session?.user?.id);
            if (error) {
                console.log(error);
                return;
            }
            const userD = data[0];
            if (userD) {
                setUserData(userD as ContextProps['userData']);
            }
        };

        getUserData();
    }, [session]);
    return (
        <AuthContext.Provider
            value={{
                user,
                session,
                userData,
            }}
        >
            {props.children}
        </AuthContext.Provider>
    );
};
const useAuth = () => React.useContext(AuthContext);
export { AuthContext, AuthProvider, useAuth };
